#pragma once
#include <COGS/Color.h>
#include <glm/common.hpp>



namespace acgm
{
  //! Representation of an image
  class Image
  {
  public:
    //! Creates an image with a specified name, up vector, and seam vector.
    Image(const std::string &image_name, const glm::vec3 &enviro_up, const glm::vec3 &enviro_seam);
    virtual ~Image() = default;

    //! Calculates color at the specified coordinates.
    cogs::Color3f GetColorAt(const glm::vec2 &uvs) const;

    //! Name of the image file.
    std::string image_name;
    //! Up vector of the environment.
    glm::vec3 enviro_up;
    //! Seam vector of the environment.
    glm::vec3 enviro_seam;

  private:
    std::vector<uint8_t> data_;
    int width_;
    int height_;
    int bpp_;
  };

}
