#pragma once

#include <COGS/Color.h>
#include <glm/common.hpp>
#include <ACGM_RayTracer_lib/Ray.h>



namespace acgm
{
  //! Input values for a shader.
  struct ShaderInput
  {
    glm::vec3 point;
    glm::vec3 normal;
    glm::vec3 dir_to_camera;
    glm::vec3 dir_to_light;
    float intensity;
  };

  //! Parameters of a shader.
  struct ShaderParams
  {
    float ambient = 0.0f;
    float diffuse = 0.0f;
    float specular = 0.0f;
    uint16_t shininess = 0;
    float glossiness = 0.0f;
    float transparency = 0.0f;
    float refractive_index = 0.0f;
  };

  //! Parameters of a material.
  struct MaterialParams
  {
    float glossiness = 0.0f;
    float transparency = 0.0f;
    float refractive_index = 0.0f;
  };


  //! Representation of a shader.
  class Shader
  {
  public:
    //! Creates a shader.
    explicit Shader();
    virtual ~Shader() = default;

    //! Calculates color based on parameters of the shader and returns the material of the object.
    virtual std::pair<cogs::Color3f, MaterialParams> CalculateColor(const ShaderInput &input) const = 0;

    //! Calculates the reflected ray.
    Ray Reflection(const glm::vec3 &ray_dir, const glm::vec3 &hit_point, const glm::vec3 &point_normal, const float &bias) const;
    //! Calculates the refracted ray.
    std::optional<Ray> Refraction(const glm::vec3 &ray_dir, const glm::vec3 &hit_point, glm::vec3 &point_normal, const float &object_refraction, const float &scene_refraction, const float &bias) const;
  };
}