#pragma once

#include <COGS/Color.h>
#include <glm/common.hpp>
#include <ACGM_RayTracer_lib/Shader.h>



namespace acgm
{
  //! Representation of a Checkerboard shader.
  class CheckerBoardShader : public Shader
  {
  public:
    //! Creates a Checkerboard shader with specified shaders and the size of the cube.
    CheckerBoardShader(const std::shared_ptr<Shader> &s0, const std::shared_ptr<Shader> &s1, const float &cube_size);
    virtual ~CheckerBoardShader() = default;

    //! Calculates color with specified input parameters.
    virtual std::pair<cogs::Color3f, MaterialParams> CalculateColor(const ShaderInput &input) const override;

  private:
    const std::shared_ptr<Shader> p0_;
    const std::shared_ptr<Shader> p1_;
    const ShaderParams params_;
    const float cube_size_;
    const float bias_ = 0.0001f;
  };
}