#pragma once

#include <glm/common.hpp>
#include <ACGM_RayTracer_lib/Light.h>



namespace acgm
{
  //! Representation of a directional light.
  class DirectionalLight : public Light
  {
  public:
    //! Create a directional light with specified intensity and direction.
    DirectionalLight(const float &intensity, const glm::vec3 &direction);
    virtual ~DirectionalLight() = default;

    //! Calculates lights intensity at the specified point.
    float GetLightIntensity(const glm::vec3 &point) const override;
    //! Calculates direction to light from the specified point.
    glm::vec3 GetDirectionToLight(const glm::vec3 &point) const override;
    //! Calculates distance to light from the specified point.
    float GetDistanceToLight(const glm::vec3 &hit_point) const override;

  private:
    glm::vec3 direction_;
  };
}