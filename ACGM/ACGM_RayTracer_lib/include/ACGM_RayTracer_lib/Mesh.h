#pragma once

#include <ACGM_RayTracer_lib/Model.h>
#include <ACGM_RayTracer_lib/Octree.h>



namespace acgm
{
  //! Representation of a mesh.
  class Mesh : public Model
  {
  public:
    //! Creates a mesh and initializes transform and octree computation if it's specified.
    Mesh(const cogs::Mesh &mesh, const cogs::Transform &transform, std::optional<std::shared_ptr<Octree>> octree = std::nullopt);
    virtual ~Mesh() = default;

    //! Function that computes intersection between model and ray.
    virtual const std::optional<acgm::HitResult> Intersection(Ray &ray) override;

    //! Calculates intersection with the triangle.
    std::optional<float> TriangleIntersection(const glm::vec3 &point0, const glm::vec3 &point1, const glm::vec3 &point2, Ray &ray);

  private:
    cogs::Transform transform_;
    cogs::Mesh mesh_;

    std::optional<std::shared_ptr<Octree>> octree_;
  };
}
