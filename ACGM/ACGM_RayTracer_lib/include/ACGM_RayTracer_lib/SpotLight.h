#pragma once

#include <glm/common.hpp>
#include <ACGM_RayTracer_lib/Light.h>



namespace acgm
{
  //! Representation of a spot light.
  class SpotLight : public Light
  {
  public:
    //! Create a spot light with specified intensity, position, range, attenuation constants, direction, cutoff angle, and exponent.
    SpotLight(const float &intensity, const glm::vec3 &position, const uint16_t &range,
      const float &linear_att, const float &quadratic, const glm::vec3 &direction, const uint16_t &cutoff, const uint16_t &exponent);
    virtual ~SpotLight() = default;

    //! Calculates lights intensity at the specified point.
    float GetLightIntensity(const glm::vec3 &point) const override;
    //! Calculates direction to light from the specified point.
    glm::vec3 GetDirectionToLight(const glm::vec3 &point) const override;
    //! Calculates distance to light from the specified point.
    float GetDistanceToLight(const glm::vec3 &hit_point) const override;

  private:
    glm::vec3 position_;
    uint16_t range_;
    float linear_att_;
    float quadratic_att_;
    glm::vec3 direction_;
    uint16_t cutoff_;
    uint16_t exponent_;
  };
}