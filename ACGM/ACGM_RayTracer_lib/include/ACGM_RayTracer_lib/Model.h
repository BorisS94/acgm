#pragma once

#include <glm/common.hpp>

#include <COGS/Color.h>
#include <COGS/Transform.h>
#include <COGS/Mesh.h>

#include <ACGM_RayTracer_lib/Ray.h>
#include <ACGM_RayTracer_lib/Shader.h>



namespace color
{
  const auto RED = cogs::Color3f(1.0f, 0.0f, 0.0f);
  const auto GREEN = cogs::Color3f(0.0f, 1.0f, 0.0f);
  const auto BLUE = cogs::Color3f(0.0f, 0.0f, 1.0f);

  const auto BLACK = cogs::Color3f(0.0f, 0.0f, 0.0f);
  const auto WHITE = cogs::Color3f(1.0f, 1.0f, 1.0f);
  const auto GREY = cogs::Color3f(0.5f, 0.5f, 0.5f);

  const auto YELLOW = cogs::Color3f(1.0f, 1.0f, 0.0f);
  const auto CYAN = cogs::Color3f(0.0f, 1.0f, 1.0f);
  const auto VIOLET = cogs::Color3f(1.0f, 0.0f, 1.0f);
}



namespace acgm
{
  //! Definition of distance and normal of the intersected point.
  struct HitResult
  {
    float t;
    glm::vec3 normal;
  };



  //! Model  - abstract base class for scene models.
  class Model
  {
  public:
    //! Creates a model.
    explicit Model();
    virtual ~Model() = default;

    //! Function that computes intersection between model and ray.
    virtual const std::optional<HitResult> Intersection(Ray &ray) = 0;

    //! Sets shader of the model.
    void SetShader(const std::shared_ptr<Shader> &shader);

    //! Returns shader of the model.
    const std::shared_ptr<Shader> GetShader() const;

  private:
    std::shared_ptr<Shader> shader_;
  };
}
