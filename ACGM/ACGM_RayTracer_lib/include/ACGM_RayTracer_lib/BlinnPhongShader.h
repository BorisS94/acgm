#pragma once

#include <COGS/Color.h>
#include <glm/common.hpp>
#include <ACGM_RayTracer_lib/Shader.h>



namespace acgm
{
  //! Representation of a BlinnPhong shader.
  class BlinnPhongShader : public Shader
  {
  public:
    //! Creates a BlinnPhong shader with specified color, shader and material parameters.
    BlinnPhongShader(const cogs::Color3f &color, const ShaderParams &params, const MaterialParams &mat_params);
    virtual ~BlinnPhongShader() = default;

    //! Calculates color with specified input parameters.
    virtual std::pair<cogs::Color3f, MaterialParams> CalculateColor(const ShaderInput &input) const override;

  private:
    cogs::Color3f color_;
    const ShaderParams params_;
    const MaterialParams mat_params_;
  };
}