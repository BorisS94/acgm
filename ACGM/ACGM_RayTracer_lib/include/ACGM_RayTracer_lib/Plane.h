#pragma once

#include <ACGM_RayTracer_lib/Model.h>



namespace acgm
{
  //! Representation of a plane.
  class Plane : public Model
  {
  public:
    //! Creates a plane with the specified reference point and normal.
    Plane(const glm::vec3 &reference, const glm::vec3 &normal);
    virtual ~Plane() = default;

    //! Function that computes the intersection between the model and the ray.
    virtual const std::optional<HitResult> Intersection(Ray &ray) override;

  private:
    glm::vec3 reference_;
    glm::vec3 normal_;
  };
}
