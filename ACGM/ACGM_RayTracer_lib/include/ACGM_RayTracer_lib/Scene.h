#pragma once

#include <vector>
#include <glm/common.hpp>
#include <HIRO_DRAW/renderers/RasterRenderer.h>

#include <ACGM_RayTracer_lib/Camera.h>
#include <ACGM_RayTracer_lib/Model.h>
#include <ACGM_RayTracer_lib/Plane.h>
#include <ACGM_RayTracer_lib/Mesh.h>
#include <ACGM_RayTracer_lib/Sphere.h>
#include <ACGM_RayTracer_lib/Light.h>
#include <ACGM_RayTracer_lib/Image.h>



namespace acgm
{
  //! Representation of the scene.
  class Scene
  {
  public:
    //! Creates a scene with specified camera, light and image.
    Scene(const std::shared_ptr<acgm::Camera> &camera, const std::shared_ptr<acgm::Light> &light, const std::shared_ptr<acgm::Image> &image);
    ~Scene() = default;

    //! RayTrace the scene.
    void Raytrace(hiro::draw::RasterRenderer &renderer) const;

    //! Adds models to the scene.
    void AddModels(std::vector<std::shared_ptr<Model>> models);

    //! Sets bias of the scene.
    void SetBias(const float &bias);

    //! Sets index of refraction for the scene.
    void SetIOR(const float &index_of_refraction);

    //! Sets maximum allowed reflections.
    void SetMaxReflections(const uint8_t &max_reflections);

    //! Sets maximum allowed refractions.
    void SetMaxRefractions(const uint8_t &max_refractions);

  private:
    std::vector<std::shared_ptr<Model>> models_;
    std::shared_ptr<acgm::Camera> camera_;
    std::shared_ptr<acgm::Light> light_;
    std::shared_ptr<acgm::Image> image_;

    float bias_ = 0.0001f;
    float index_of_refraction_ = 1.0f;

    uint8_t max_reflections_ = 10;
    uint8_t max_refractions_ = 10;

    // Returns color calculated by the reflections and refractions.
    cogs::Color3f Trace(Ray &ray, const uint8_t &depth_reflection, const uint8_t &depth_refractions) const;
    // Computes UV coordinates.
    glm::vec2 ComputeUV(const glm::vec3 &ray_direction) const;

    // Checks if point is in the shadow.
    const bool IsShadow(const glm::vec3 &hit_point) const;
    // Checks if point is in the view plane.
    const bool IsInViewPlane(const float &t) const;

    // Converts coordinates from raster to screen space.
    glm::vec2 RasterToScreenSpace(const glm::uvec2 &raster_space, const glm::uvec2 &resolution) const;
    // Convers coordinates from screen to camera space.
    glm::vec2 ScreenToCameraSpace(const glm::vec2 &screen_space, const glm::uvec2 &resolution) const;

    // Returns direction for the specifiec camera space coordinates.
    glm::vec3 GetDirection(const glm::vec2 &camera_space) const;
  };
}
