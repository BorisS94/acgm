#pragma once

#include <glm/common.hpp>



namespace acgm
{
  enum class LightType
  {
    directional,
    point,
    spot
  };



  //! Representation of a light.
  class Light
  {
  public:
    //! Create a light.
    explicit Light(const float &intensity);
    virtual ~Light() = default;

    //! Calculates lights intensity at the specified point.
    virtual float GetLightIntensity(const glm::vec3 &point) const = 0;
    //! Calculates direction to light from the specified point.
    virtual glm::vec3 GetDirectionToLight(const glm::vec3 &point) const = 0;
    //! Calculates distance to light from the specified point.
    virtual float GetDistanceToLight(const glm::vec3 &hit_point) const = 0;

    //! Sets intensity of the light.
    void SetIntensity(const float &intensity);
    //! Returns intensity of the light.
    const float &GetIntensity() const;

    //! Calculates distance between two points.
    float CalculateDistance(const glm::vec3 &point0, const glm::vec3 &point1) const;

  private:
    float intensity_;
  };
}