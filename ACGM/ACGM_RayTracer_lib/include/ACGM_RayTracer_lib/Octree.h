#pragma once

#include <COGS/Mesh.h>

#include <ACGM_RayTracer_lib/Ray.h>
#include <ACGM_RayTracer_lib/Model.h>



namespace acgm
{
  //! Representation of an Octree node.
  class OctreeNode : public std::enable_shared_from_this<OctreeNode>
  {
  public:
    //! Creates an Octree node.
    OctreeNode(const glm::vec3 &center, const glm::vec3 &size, const uint32_t level, std::shared_ptr<OctreeNode> parent = nullptr);
    virtual ~OctreeNode() = default;

    //! Function that computes intersection between model and ray.
    const std::optional<HitResult> Intersection(Ray &ray);
    //! Calculates intersection with the triangle.
    std::optional<HitResult> TriangleIntersection(const glm::vec3 &point0, const glm::vec3 &point1, const glm::vec3 &point2, Ray &ray);
    //! Adds triangle to current octree.
    void AddTriangle(const std::vector<glm::vec3> &triangle);

    //! Checks if the triangle is in the AABB.
    bool IsTriangleInAABB(const glm::vec3 &point0, const glm::vec3 &point1, const glm::vec3 &point2, const glm::vec3 &center, const glm::vec3 &size);
    //! Checks if the point is in the AABB.
    bool IsPointInAABB(const glm::vec3 &point, const glm::vec3 &center, const glm::vec3 &size) const;
    //! Checks if the ray intersects with the AABB.
    bool AABBIntersection(Ray &ray, const glm::vec3 &center, const glm::vec3 &size) const;

  private:
    glm::vec3 center_;
    glm::vec3 size_;
    uint32_t level_;
    std::shared_ptr<OctreeNode> parent_;
    std::vector<std::shared_ptr<OctreeNode>> child_;
    std::vector<glm::vec3> child_center_;

    std::vector<std::vector<glm::vec3>> traingles_;

    bool is_limit_ = false;

    const static uint32_t level = 5;
    const static uint32_t limit = 4;
  };



  //! Representation of an Octree structure.
  class Octree
  {
  public:
    //! Creates an Octree.
    Octree() = default;
    virtual ~Octree() = default;

    //! Calculates the center and size of the mesh's octree.
    void SetupOctree(const cogs::Mesh &mesh);

    //! Function that computes intersection between model and ray.
    const std::optional<HitResult> Intersection(Ray &ray);
    //! Adds triangle to current octree.
    void AddTriangle(std::vector<glm::vec3> &triangle);

  private:
    cogs::Mesh mesh_;
    std::shared_ptr<OctreeNode> root_;

    void SetCenterAndSize();
  };
}
