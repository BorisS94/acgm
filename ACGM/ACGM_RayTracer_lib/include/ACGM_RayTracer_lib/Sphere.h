#pragma once

#include <ACGM_RayTracer_lib/Model.h>



namespace acgm
{
  //! Representation of a sphere.
  class Sphere : public Model
  {
  public:
    //! Creates a sphere with the specified position and radius.
    Sphere(const glm::vec3 &position, float radius);
    virtual ~Sphere() = default;

    //! Function that computes the intersection between the model and the ray.
    virtual const std::optional<HitResult> Intersection(acgm::Ray &ray) override;

    //! Returns position of the spehere.
    const glm::vec3 &GetPosition() const;
    //! Returns radius of the sphere.
    float GetRadius() const;

  private:
    glm::vec3 position_;
    float radius_;
  };
}
