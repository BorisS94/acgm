#pragma once

#include <optional>
#include <glm/common.hpp>



namespace acgm
{
  //! Representation of a ray
  class Ray
  {
  public:
    //! Creates a ray with specified origin.
    Ray(const glm::vec3 &origin_);
    virtual ~Ray() = default;

    //! Returns the origin of the ray.
    const glm::vec3 &GetOrigin() const;
    //! Returns the diretion of the ray.
    const glm::vec3 &GetDirection() const;

    //! Sets direction of the ray.
    void SetDirection(const glm::vec3 &direction);

  private:
    glm::vec3 origin_;
    glm::vec3 direction_;
  };

}
