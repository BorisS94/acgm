#pragma once

#include <vector>
#include <Utils/ExtGLM.h>
#include <glm/common.hpp>



namespace acgm
{
  //! Definition of camera parameters.
  struct CameraParams
  {
    //! Position of the camera.
    glm::vec3 position;
    //! Target vector of the camera.
    glm::vec3 forward;
    //! Up vector of the camera.
    glm::vec3 up;
    //! Fov of the camera.
    float fov = 1.0472f;
    //! Near plane.
    float near_plane = 0.01f;
    //! Far plane.
    float far_plane = 1000.0f;
  };



  //! Camera representation.
  class Camera
  {
  public:
    //! Camera constructor.
    Camera(const CameraParams &params);
    virtual ~Camera() = default;

    //! Get camera position.
    const glm::vec3 &GetPosition() const;
    //! Get camera up vector.
    const glm::vec3 &GetUp() const;
    //! Get camera look at vector.
    const glm::vec3 &GetLookAt() const;
    //! Get camera right vector.
    const glm::vec3 &GetRight() const;
    //! Get camera fov.
    const float &GetFov() const;
    //! Get near plane.
    const float &GetNearPlane() const;
    //! Get far plane.
    const float &GetFarPlane() const;

    //! Set camera fov.
    void SetFov(const float &fov);
    //! Set near plane.
    void SetNearPlane(const float &near_plane);
    //! Set far plane.
    void SetFarPlane(const float &far_plane);

  private:
    // Camera parameters.
    CameraParams params_;
    // Right vector of camera.
    glm::vec3 right_;
  };
}
