#pragma once

#include <glm/common.hpp>
#include <ACGM_RayTracer_lib/Light.h>



namespace acgm
{
  //! Representation of a point light.
  class PointLight : public Light
  {
  public:
    //! Create a point light with specified intensity, position, range, and attenuation constants.
    PointLight(const float &intensity, const glm::vec3 &position, const uint16_t &range, const float &linear_att, const float &quadratic);
    virtual ~PointLight() = default;

    //! Calculates lights intensity at the specified point.
    float GetLightIntensity(const glm::vec3 &point) const override;
    //! Calculates direction to light from the specified point.
    glm::vec3 GetDirectionToLight(const glm::vec3 &point) const override;
    //! Calculates distance to light from the specified point.
    float GetDistanceToLight(const glm::vec3 &hit_point) const override;

  private:
    glm::vec3 position_;
    uint16_t range_;
    float linear_att_;
    float quadratic_att_;
  };
}