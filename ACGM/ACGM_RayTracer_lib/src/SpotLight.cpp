#include <glm/glm.hpp>
#include <Utils/ExtGLM.h>
#include <ACGM_RayTracer_lib/SpotLight.h>



acgm::SpotLight::SpotLight(const float &intensity, const glm::vec3 &position, const uint16_t &range,
  const float &linear_att, const float &quadratic, const glm::vec3 &direction, const uint16_t &cutoff, const uint16_t &exponent)
  : Light(intensity),
    position_(position),
    range_(range),
    linear_att_(linear_att),
    quadratic_att_(quadratic),
    direction_(direction),
    cutoff_(cutoff),
    exponent_(exponent)
{

}

glm::vec3 acgm::SpotLight::GetDirectionToLight(const glm::vec3 &point) const
{
  return glm::normalize(position_ - point);
}

float acgm::SpotLight::GetLightIntensity(const glm::vec3 &point) const
{
  const auto point_dir = glm::normalize(point - position_);

  const auto dot = glm::dot(direction_, point_dir);
  const auto len_light = glm::dot(direction_, direction_);
  const auto len_point = glm::dot(point_dir, point_dir);

  auto alpha = std::acos(dot / std::sqrt(len_light * len_point));
  alpha = alpha * 180 / glm::pi<float>();

  if (glm_ext::IsGequal(alpha, cutoff_))
  {
    return 0.0f;
  }

  float decay = 1 - pow(alpha / cutoff_, exponent_);

  const float distance = this->CalculateDistance(position_, point);
  const float l = (range_) / (range_ + (distance * linear_att_));
  const float q = pow(range_, 2) / (pow(range_, 2) + (pow(distance, 2) * quadratic_att_));

  const float intensity = l * q * this->GetIntensity();

  return intensity * decay;
}

float acgm::SpotLight::GetDistanceToLight(const glm::vec3 &hit_point) const
{
  return this->CalculateDistance(position_, hit_point);
}
