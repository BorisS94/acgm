#include <ACGM_RayTracer_lib/Mesh.h>



acgm::Mesh::Mesh(const cogs::Mesh &mesh, const cogs::Transform &transform, std::optional<std::shared_ptr<Octree>> octree)
  : mesh_(mesh),
    transform_(transform),
    octree_(octree)
{
  mesh_.points->Transform(transform);

  if (octree_.has_value())
  {
    octree_.value()->SetupOctree(mesh_);
  }
}

const std::optional<acgm::HitResult> acgm::Mesh::Intersection(Ray &ray)
{
  // If an octree is defined, then use it to calculate intersection.
  if (octree_.has_value())
  {
    return octree_.value()->Intersection(ray);
  }

  std::optional<float> help_t = std::nullopt;
  acgm::HitResult intersection;
  intersection.t = std::numeric_limits<float>::max();
  intersection.normal = glm::vec3(0.0f, 0.0f, 0.0f);

  glm::uvec3 hit_triangle = glm::uvec3(0, 0, 0);

  for (uint32_t i = 0; i < mesh_.faces->GetFaceCount(); i++)
  {
    const auto triangle = mesh_.faces->GetFaces().at(i);
    const auto positions = mesh_.points->GetPositions();

    const auto point0 = positions[triangle.x];
    const auto point1 = positions[triangle.y];
    const auto point2 = positions[triangle.z];

    help_t = TriangleIntersection(point0, point1, point2, ray);
    if (!help_t.has_value())
    {
      continue;
    }
    else
    {
      if (glm_ext::IsGreater(intersection.t, help_t.value()) && glm_ext::IsGreater(help_t.value(), 0.001))
      {
        intersection.t = help_t.value();
        hit_triangle = triangle;
      }
    }
  }

  if (glm_ext::IsLequal(intersection.t, 0.0001f) || glm_ext::IsGequal(intersection.t, std::numeric_limits<float>::max()))
  {
    return std::nullopt;
  }

  const auto positions = mesh_.points->GetPositions();
  const auto v1 = positions[hit_triangle.y] - positions[hit_triangle.x];
  const auto v2 = positions[hit_triangle.z] - positions[hit_triangle.x];
  intersection.normal = glm::normalize(glm::cross(v1, v2));

  return intersection;
}

std::optional<float> acgm::Mesh::TriangleIntersection(const glm::vec3 &point0, const glm::vec3 &point1, const glm::vec3 &point2, Ray &ray)
{
  auto AB = point1 - point0;
  auto AC = point2 - point0;

  auto pvec = glm::cross(ray.GetDirection(), AC);

  float det = glm::dot(pvec, AB);

  if (glm_ext::IsLess(det, 0.0001f) && glm_ext::IsGreater(det, -0.0001f))
  { return std::nullopt; }

  float invDet = 1.0f / det;

  auto tvec = ray.GetOrigin() - point0;

  float u = glm::dot(tvec, pvec) * invDet;

  if (glm_ext::IsLess(u, 0.0f) || glm_ext::IsGreater(u, 1.0f))
  { return std::nullopt; }

  auto qvec = cross(tvec, AB);

  float v = glm::dot(ray.GetDirection(), qvec) * invDet;

  if (glm_ext::IsLess(v, 0.0f) || glm_ext::IsGreater((u + v), 1.0f))
  { return std::nullopt; }

  return glm::dot(AC, qvec) * invDet;
}
