#include <ACGM_RayTracer_lib/Camera.h>



acgm::Camera::Camera(const CameraParams &params)
  : params_(params)
{
  right_ = glm::cross(params_.forward, params.up);
}

const glm::vec3 &acgm::Camera::GetPosition() const
{
  return params_.position;
}

const glm::vec3 &acgm::Camera::GetUp() const
{
  return params_.up;
}

const glm::vec3 &acgm::Camera::GetLookAt() const
{
  return params_.forward;
}

const glm::vec3 &acgm::Camera::GetRight() const
{
  return right_;
}


const float &acgm::Camera::GetFov() const
{
  return params_.fov;
}

const float &acgm::Camera::GetNearPlane() const
{
  return params_.near_plane;
}

const float &acgm::Camera::GetFarPlane() const
{
  return params_.far_plane;
}

void acgm::Camera::SetFov(const float &fov)
{
  params_.fov = fov;
}

void acgm::Camera::SetNearPlane(const float &near_plane)
{
  params_.near_plane = near_plane;
}

void acgm::Camera::SetFarPlane(const float &far_plane)
{
  params_.far_plane = far_plane;
}
