#include <glm/glm.hpp>
#include <Utils/ExtGLM.h>
#include <ACGM_RayTracer_lib/CheckerboardShader.h>



acgm::CheckerBoardShader::CheckerBoardShader(const std::shared_ptr<Shader> &s0, const std::shared_ptr<Shader> &s1, const float &cube_size)
  : p0_(s0),
    p1_(s1),
    cube_size_(cube_size)
{
}

std::pair<cogs::Color3f, acgm::MaterialParams> acgm::CheckerBoardShader::CalculateColor(const ShaderInput &input) const
{
  const uint16_t points = std::floor(input.point.x / cube_size_ + bias_) + std::floor(input.point.y / cube_size_ + bias_) + std::floor(input.point.z / cube_size_ + bias_);
  if (points % 2 == 0)
  {
    return p0_->CalculateColor(input);
  }
  else
  {
    return p1_->CalculateColor(input);
  }
}
