#include <thread>

#include <glm/geometric.hpp>
#include <glm/gtx/vector_angle.hpp>

#include <ACGM_RayTracer_lib/Scene.h>
#include <ACGM_RayTracer_lib/Shader.h>



acgm::Scene::Scene(const std::shared_ptr<acgm::Camera> &camera, const std::shared_ptr<acgm::Light> &light, const std::shared_ptr<acgm::Image> &image)
  : camera_(camera),
    light_(light),
    image_(image)
{

}

void acgm::Scene::Raytrace(hiro::draw::RasterRenderer &renderer) const
{
  const auto resolution = renderer.GetResolution();

  const clock_t begin_time = clock();
  #pragma omp parallel for num_threads(std::thread::hardware_concurrency())
  for (int32_t x = 0; x < resolution.x; x++)
  {
    for (int32_t y = 0; y < resolution.y; y++)
    {
      auto color = color::BLACK;

      const auto screen_space = RasterToScreenSpace(glm::uvec2(x, y), resolution);
      const auto camera_space = ScreenToCameraSpace(screen_space, resolution);
      const auto ray_direction = GetDirection(camera_space);

      Ray ray(camera_->GetPosition());
      ray.SetDirection(ray_direction);

      // Recursive ray-tracing
      color = Trace(ray, 0, 0);

      renderer.SetPixel(x, y, color);
    }
  }
  std::cout << "CPU computation time: " << static_cast<float>((clock() - begin_time)) / CLOCKS_PER_SEC << std::endl;
}

cogs::Color3f acgm::Scene::Trace(Ray &ray, const uint8_t &depth_reflections, const uint8_t &depth_refractions) const
{
  if (depth_reflections > max_reflections_ || depth_refractions > max_refractions_)
  {
    return cogs::Color3f(0.0f, 0.0f, 0.0f);
  }

  auto t = std::numeric_limits<float>::max();
  auto point_normal = glm::vec3(0.0f, 0.0f, 0.0f);
  uint16_t model_id = 0;

  // Calculates an intersection for each model in the scene.
  for (uint16_t p = 0; p < models_.size(); p++)
  {
    std::optional<acgm::HitResult> intersection;
    intersection = models_.at(p)->Intersection(ray);
    if (intersection.has_value())
    {
      if (IsInViewPlane(intersection.value().t) && intersection.value().t < t)
      {
        t = intersection.value().t;
        point_normal = intersection.value().normal;
        model_id = p;
      }
    }
  }

  if (glm_ext::IsLess(t, std::numeric_limits<float>::max()))
  {
    const auto ray_dir = ray.GetDirection();
    const auto hit_point = ray.GetOrigin() + ray_dir * t;
    const auto intensity = IsShadow(hit_point - ray_dir * bias_) ? 0.0f : light_->GetLightIntensity(hit_point);

    acgm::ShaderInput shader_input;
    shader_input.point = hit_point;
    shader_input.normal = point_normal;
    shader_input.dir_to_camera = -ray_dir;
    shader_input.dir_to_light = light_->GetDirectionToLight(hit_point);
    shader_input.intensity = intensity;

    const auto shader = models_.at(model_id)->GetShader();
    const auto shader_color = shader->CalculateColor(shader_input);
    const auto color = shader_color.first;
    const auto mat_params = shader_color.second;

    // No reflection or refraction
    if (glm_ext::IsLequal(mat_params.glossiness, 0.05f) && glm_ext::IsLequal(mat_params.transparency, 0.05f))
    {
      return color;
    }
    // Only reflection
    else if (glm_ext::IsLequal(mat_params.transparency, 0.05f))
    {
      return (1 - mat_params.glossiness) * color + mat_params.glossiness * Trace(shader->Reflection(ray_dir, hit_point, point_normal, bias_), depth_reflections + 1, depth_refractions);
    }
    // Only refraction
    else if (glm_ext::IsLequal(mat_params.glossiness, 0.05f))
    {
      auto refraction = shader->Refraction(ray_dir, hit_point, point_normal, mat_params.refractive_index, index_of_refraction_, bias_);
      if (refraction.has_value())
      {
        return (1 - mat_params.transparency) * color + mat_params.transparency * Trace(refraction.value(), depth_reflections, depth_refractions + 1);
      }
      else //Total Internal Reflection
      {
        return (1 - mat_params.transparency) * color + mat_params.transparency * Trace(shader->Reflection(ray_dir, hit_point, point_normal, bias_), depth_reflections + 1, depth_refractions);
      }
    }
    // Reflection and refraction
    else
    {
      auto refraction = shader->Refraction(ray_dir, hit_point, point_normal, mat_params.refractive_index, index_of_refraction_, bias_);
      if (refraction.has_value())
      {
        return ((1 - mat_params.glossiness - mat_params.transparency)) * color + (mat_params.glossiness + mat_params.transparency) * Trace(shader->Reflection(ray_dir, hit_point, point_normal, bias_), depth_reflections + 1, depth_refractions) + (mat_params.glossiness + mat_params.transparency) * Trace(refraction.value(), depth_reflections, depth_refractions + 1);
      }
      else
      {
        return ((1 - mat_params.glossiness - mat_params.transparency)) * color + (mat_params.glossiness + mat_params.transparency) * Trace(shader->Reflection(ray_dir, hit_point, point_normal, bias_), depth_reflections + 1, depth_refractions);
      }
    }
  }
  else
  {
    if (!(image_->image_name.empty()))
    {
      return image_->GetColorAt(ComputeUV(ray.GetDirection()));
    }
  }

  return cogs::Color3f(0.0f, 0.0f, 0.0f);
}

glm::vec2 acgm::Scene::ComputeUV(const glm::vec3 &ray_direction) const
{
  const auto up = glm::normalize(image_->enviro_up);
  const auto seam = glm::normalize(image_->enviro_seam);
  const auto view = glm::normalize(ray_direction);
  const auto X = glm::normalize(ray_direction - glm::dot(image_->enviro_up, ray_direction) * image_->enviro_up);

  const auto latitude = std::acos(glm::dot(view, up));
  const auto longitude = glm::orientedAngle(X, seam, up);

  const auto u = latitude / glm::pi<float>();
  const auto v = (longitude - (-glm::pi<float>())) / (glm::pi<float>() - (-glm::pi<float>()));

  return glm::vec2(u, v);
}

const bool acgm::Scene::IsShadow(const glm::vec3 &hit_point) const
{
  auto light_direction = light_->GetDirectionToLight(hit_point);

  Ray light_ray(hit_point);
  light_ray.SetDirection(light_direction);

  for (uint16_t p = 0; p < models_.size(); p++)
  {
    const auto t_light = models_.at(p)->Intersection(light_ray);
    const auto distance = light_->GetDistanceToLight(hit_point);
    // If the ray intersects any model that is between point and light, then the point is in shadow.
    if (t_light.has_value() && t_light.value().t < distance)
    {
      return true;
    }
  }

  return false;
}

const bool acgm::Scene::IsInViewPlane(const float &t) const
{
  if (glm_ext::IsGreater(t, camera_->GetFarPlane()) || glm_ext::IsLess(t, camera_->GetNearPlane()))
  {
    return false;
  }

  return true;
}

glm::vec2 acgm::Scene::RasterToScreenSpace(const glm::uvec2 &raster_space, const glm::uvec2 &resolution) const
{
  const auto device_space = glm::vec2((raster_space.x + 0.5) / resolution.x, (raster_space.y + 0.5) / resolution.y);

  return glm::vec2(1 - (2 * device_space.x), (2 * device_space.y - 1));
}

glm::vec2 acgm::Scene::ScreenToCameraSpace(const glm::vec2 &screen_space, const glm::uvec2 &resolution) const
{
  const auto fov = camera_->GetFov();
  const auto aspect = static_cast<float>(resolution.x) / static_cast<float>(resolution.y);

  return glm::vec2(screen_space.x * (-aspect * std::tan(fov / 2)), screen_space.y * std::tan(fov / 2));
}

glm::vec3 acgm::Scene::GetDirection(const glm::vec2 &camera_space) const
{
  return glm::normalize(camera_->GetLookAt() + camera_space.x * camera_->GetRight() + camera_space.y * camera_->GetUp());
}

void acgm::Scene::AddModels(std::vector<std::shared_ptr<Model>> models)
{
  models_ = models;
}

void acgm::Scene::SetBias(const float &bias)
{
  bias_ = bias;
}

void acgm::Scene::SetIOR(const float &index_of_refraction)
{
  index_of_refraction_ = index_of_refraction;
}

void acgm::Scene::SetMaxReflections(const uint8_t &max_reflections)
{
  max_reflections_ = max_reflections;
}

void acgm::Scene::SetMaxRefractions(const uint8_t &max_refractions)
{
  max_refractions_ = max_refractions;
}
