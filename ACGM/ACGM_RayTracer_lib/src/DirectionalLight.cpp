#include <glm/glm.hpp>
#include <Utils/ExtGLM.h>
#include <ACGM_RayTracer_lib/DirectionalLight.h>



acgm::DirectionalLight::DirectionalLight(const float &intensity, const glm::vec3 &direction)
  : Light(intensity),
    direction_(direction)
{
}

glm::vec3 acgm::DirectionalLight::GetDirectionToLight(const glm::vec3 &point) const
{
  return glm::normalize(-direction_);
}

float acgm::DirectionalLight::GetLightIntensity(const glm::vec3 &point) const
{
  return this->GetIntensity();
}

float acgm::DirectionalLight::GetDistanceToLight(const glm::vec3 &hit_point) const
{
  return std::numeric_limits<float>::max();
}
