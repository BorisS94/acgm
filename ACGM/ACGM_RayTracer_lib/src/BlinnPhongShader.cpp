#include <glm/glm.hpp>
#include <Utils/ExtGLM.h>
#include <ACGM_RayTracer_lib/BlinnPhongShader.h>



acgm::BlinnPhongShader::BlinnPhongShader(const cogs::Color3f &color, const ShaderParams &params, const MaterialParams &mat_params)
  : color_(color),
    params_(params),
    mat_params_(mat_params)
{
}

std::pair<cogs::Color3f, acgm::MaterialParams> acgm::BlinnPhongShader::CalculateColor(const ShaderInput &input) const
{
  if (glm_ext::AreEqual(input.intensity, 0.0f))
  {
    return std::make_pair(params_.ambient * color_, mat_params_);
  }

  // Diffuse: Kd*Ld*(l.n)
  const float cos_diff = glm::dot(input.dir_to_light, input.normal);
  const auto diffuse = params_.diffuse * input.intensity * cos_diff;

  // Specular: Ks*Ls*(h.n)^Ns
  // h: normalize(l + v)
  const auto h = glm::normalize(input.dir_to_light + input.dir_to_camera);
  const auto cos_spec = glm::pow(glm::dot(h, input.normal), params_.shininess);
  const auto specular = params_.specular * input.intensity * cos_spec;

  return std::make_pair((params_.ambient + diffuse) * color_ + specular * cogs::Color3f(1.0f, 1.0f, 1.0f), mat_params_);
}
