#include <ACGM_RayTracer_lib/Ray.h>



acgm::Ray::Ray(const glm::vec3 &origin)
  : origin_(origin)
{
}

const glm::vec3 &acgm::Ray::GetOrigin() const
{
  return origin_;
}

const glm::vec3 &acgm::Ray::GetDirection() const
{
  return direction_;
}

void acgm::Ray::SetDirection(const glm::vec3 &direction)
{
  direction_ = direction;
}
