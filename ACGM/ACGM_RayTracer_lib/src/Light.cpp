#include <glm/glm.hpp>
#include <Utils/ExtGLM.h>
#include <ACGM_RayTracer_lib/Light.h>



acgm::Light::Light(const float &intensity)
  : intensity_(intensity)
{
}

void acgm::Light::SetIntensity(const float &intensity)
{
  intensity_ = intensity;
}

const float &acgm::Light::GetIntensity() const
{
  return intensity_;
}

float acgm::Light::CalculateDistance(const glm::vec3 &point0, const glm::vec3 &point1) const
{
  float distance, tempx, tempy, tempz;
  tempx = (point0.x - point1.x);
  tempx = tempx * tempx;
  tempy = (point0.y - point1.y);
  tempy = tempy * tempy;
  tempz = (point0.z - point1.z);
  tempz = tempz * tempz;
  distance = tempx + tempy + tempz;
  distance = sqrt(distance);

  return distance;
}
