#include <ACGM_RayTracer_lib/Plane.h>



acgm::Plane::Plane(const glm::vec3 &reference, const glm::vec3 &normal)
  : reference_(reference),
    normal_(normal)
{
}

const std::optional<acgm::HitResult> acgm::Plane::Intersection(Ray &ray)
{
  float dot = glm::dot(ray.GetDirection(), normal_);

  if (glm_ext::AreEqual(dot, 0.0f))
  {
    return std::nullopt;
  }

  acgm::HitResult intersection;
  intersection.t = glm::dot(reference_ - ray.GetOrigin(), normal_) / dot;
  intersection.normal = normal_;

  if (glm_ext::IsLequal(intersection.t, 0.0001f))
  {
    return std::nullopt;
  }

  return intersection;
}
