#include <glm/geometric.hpp>
#include <glm/gtc/epsilon.hpp>

#include <ACGM_RayTracer_lib/Scene.h>
#include <ACGM_RayTracer_lib/Sphere.h>



acgm::Sphere::Sphere(const glm::vec3 &position, float radius)
  : position_(position), radius_(radius)
{
}

const std::optional<acgm::HitResult> acgm::Sphere::Intersection(acgm::Ray &ray)
{
  const auto ray_origin = ray.GetOrigin();
  const auto ray_direction = ray.GetDirection();

  glm::vec3 oo = ray_origin - position_;

  float A = glm::dot(ray_direction, ray_direction);
  float B = -2.0f * glm::dot(oo, ray_direction);
  float C = glm::dot(oo, oo) - radius_ * radius_;
  float D = B * B - 4.0f * A * C;

  if (D < 0)
  {
    return std::nullopt;
  }

  float sD = glm::sqrt(D);

  float t1 = 0.5 * (B + sD) / A;
  if (t1 < 0.0001f)
  {
    t1 = INFINITY;
  }

  float t2 = 0.5 * (B - sD) / A;
  if (t2 < 0.0001f)
  {
    t2 = INFINITY;
  }

  const float t = glm::min(t1, t2);
  if (glm::isinf(t) || t < 0.0f)
  {
    return std::nullopt;
  }

  HitResult hit;
  hit.t = t;
  const auto point = ray_origin + ray_direction * t;
  hit.normal = glm::normalize(point - position_);
  return hit;
}

const glm::vec3 &acgm::Sphere::GetPosition() const
{
  return position_;
}

float acgm::Sphere::GetRadius() const
{
  return radius_;
}
