#include <ACGM_RayTracer_lib/Image.h>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image/stb_image.h"



acgm::Image::Image(const std::string &image_name, const glm::vec3 &enviro_up, const glm::vec3 &enviro_seam)
  : image_name(image_name),
    enviro_up(enviro_up),
    enviro_seam(enviro_seam)
{
  uint8_t *data = stbi_load(image_name.c_str(), &width_, &height_, &bpp_, 3);
  data_ = std::vector<uint8_t>(data, data + width_ * height_ * bpp_);
  stbi_image_free(data);
}

cogs::Color3f acgm::Image::GetColorAt(const glm::vec2 &uvs) const
{
  const auto index = bpp_ * ((static_cast<uint16_t>(uvs.x * height_)) * width_ + (static_cast<uint16_t>(uvs.y * width_)));

  const auto r = static_cast<float>(data_[index + 0]) / 255.0f;
  const auto g = static_cast<float>(data_[index + 1]) / 255.0f;
  const auto b = static_cast<float>(data_[index + 2]) / 255.0f;

  return cogs::Color3f(r, g, b);
}
