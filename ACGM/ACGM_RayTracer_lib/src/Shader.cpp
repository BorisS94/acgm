#include <glm/glm.hpp>
#include <Utils/ExtGLM.h>
#include <ACGM_RayTracer_lib/Shader.h>



acgm::Shader::Shader()
{
}

acgm::Ray acgm::Shader::Reflection(const glm::vec3 &ray_dir, const glm::vec3 &hit_point, const glm::vec3 &point_normal, const float &bias) const
{
  const auto dir_reflected = 2 * glm::dot(-ray_dir, point_normal) * point_normal + ray_dir;
  Ray reflected_ray(hit_point + point_normal * bias);
  reflected_ray.SetDirection(dir_reflected);

  return reflected_ray;
}

std::optional<acgm::Ray> acgm::Shader::Refraction(const glm::vec3 &ray_dir, const glm::vec3 &hit_point, glm::vec3 &point_normal, const float &object_refraction, const float &scene_refraction, const float &bias) const
{
  bool is_inside = false;

  float theta_1 = glm::dot(ray_dir, point_normal);

  if (glm::dot(ray_dir, point_normal) < 0)
  {
    theta_1 = -theta_1;
  }
  else
  {
    point_normal = -point_normal;
    is_inside = true;
  }

  float n = 0.0f;
  if (is_inside)
  {
    n = object_refraction / scene_refraction;
  }
  else
  {
    n = scene_refraction / object_refraction;
  }

  const auto theta_2 = 1 - (n * n) * (1 - (theta_1 * theta_1));
  if (theta_2 < 0)
  {
    return std::nullopt;
  }

  const auto dir_refracted = n * ray_dir + point_normal * (n * theta_1 - std::sqrt(theta_2));
  Ray refracted_ray(hit_point - point_normal * bias);
  refracted_ray.SetDirection(dir_refracted);

  return refracted_ray;
}
