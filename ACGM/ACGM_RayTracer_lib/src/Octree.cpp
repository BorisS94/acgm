#include <ACGM_RayTracer_lib/Octree.h>



acgm::OctreeNode::OctreeNode(const glm::vec3 &center, const glm::vec3 &size, const uint32_t level, std::shared_ptr<OctreeNode> parent)
  : center_(center),
    size_(size),
    level_(level),
    parent_(parent)
{
  const auto child_center = glm::vec3(center_.x, center_.y, center_.z);
  child_center_.reserve(8);
  child_center_.push_back(glm::vec3(child_center.x - size.x / 2.0, child_center.y + size.y / 2.0, child_center.z - size.z / 2.0));
  child_center_.push_back(glm::vec3(child_center.x + size.x / 2.0, child_center.y + size.y / 2.0, child_center.z - size.z / 2.0));
  child_center_.push_back(glm::vec3(child_center.x - size.x / 2.0, child_center.y - size.y / 2.0, child_center.z - size.z / 2.0));
  child_center_.push_back(glm::vec3(child_center.x + size.x / 2.0, child_center.y - size.y / 2.0, child_center.z - size.z / 2.0));
  child_center_.push_back(glm::vec3(child_center.x - size.x / 2.0, child_center.y + size.y / 2.0, child_center.z + size.z / 2.0));
  child_center_.push_back(glm::vec3(child_center.x + size.x / 2.0, child_center.y + size.y / 2.0, child_center.z + size.z / 2.0));
  child_center_.push_back(glm::vec3(child_center.x - size.x / 2.0, child_center.y - size.y / 2.0, child_center.z + size.z / 2.0));
  child_center_.push_back(glm::vec3(child_center.x + size.x / 2.0, child_center.y - size.y / 2.0, child_center.z + size.z / 2.0));

  child_.resize(8);
}

const std::optional<acgm::HitResult> acgm::OctreeNode::Intersection(Ray &ray)
{
  auto half_size = size_ / 2.0f;

  std::optional<acgm::HitResult> help_t = std::nullopt;
  acgm::HitResult intersection;
  intersection.t = std::numeric_limits<float>::max();
  intersection.normal = glm::vec3(0.0f, 0.0f, 0.0f);

  if (AABBIntersection(ray, center_, size_))
  {
    if (!is_limit_ || level_ == level)
    {
      for (std::vector<glm::vec3> surface : traingles_)
      {
        help_t = TriangleIntersection(surface.at(0), surface.at(1), surface.at(2), ray);

        if (!help_t.has_value())
        {
          continue;
        }
        else
        {
          if (glm_ext::IsGreater(intersection.t, help_t.value().t) && glm_ext::IsGreater(help_t.value().t, 0.001))
          {
            intersection.t = help_t.value().t;
            intersection.normal = help_t.value().normal;
          }
        }
      }
    }
    else
    {
      std::vector<uint8_t> cube_order_;
      cube_order_.reserve(8);

      for (uint8_t i = 0; i < 8; i++)
      {
        if (child_.at(i) && AABBIntersection(ray, child_center_.at(i), half_size))
        {
          cube_order_.push_back(i);
        }
      }

      for (uint8_t order : cube_order_)
      {
        if (child_.at(order))
        {
          std::optional<acgm::HitResult> help_t = child_.at(order)->Intersection(ray);
          if (!help_t.has_value())
          {
            continue;
          }
          else
          {
            if (glm_ext::IsGreater(intersection.t, help_t.value().t) && glm_ext::IsGreater(help_t.value().t, 0.001))
            {
              intersection.t = help_t.value().t;
              intersection.normal = help_t.value().normal;
            }
          }
        }
      }
    }
  }

  if (glm_ext::IsLequal(intersection.t, 0.0001f) || glm_ext::IsGequal(intersection.t, std::numeric_limits<float>::max()))
  {
    return std::nullopt;
  }

  return intersection;
}

std::optional<acgm::HitResult> acgm::OctreeNode::TriangleIntersection(const glm::vec3 &point0, const glm::vec3 &point1, const glm::vec3 &point2, acgm::Ray &ray)
{
  auto AB = point1 - point0;
  auto AC = point2 - point0;

  auto pvec = glm::cross(ray.GetDirection(), AC);

  float det = glm::dot(pvec, AB);

  if (glm_ext::IsLess(det, 0.0001f) && glm_ext::IsGreater(det, -0.0001f))
  {
    return std::nullopt;
  }

  float invDet = 1.0f / det;

  auto tvec = ray.GetOrigin() - point0;

  float u = glm::dot(tvec, pvec) * invDet;

  if (glm_ext::IsLess(u, 0.0f) || glm_ext::IsGreater(u, 1.0f))
  {
    return std::nullopt;
  }

  auto qvec = cross(tvec, AB);

  float v = glm::dot(ray.GetDirection(), qvec) * invDet;

  if (glm_ext::IsLess(v, 0.0f) || glm_ext::IsGreater((u + v), 1.0f))
  {
    return std::nullopt;
  }

  acgm::HitResult intersection;
  intersection.t = glm::dot(AC, qvec) * invDet;
  const auto v1 = point1 - point0;
  const auto v2 = point2 - point0;
  const auto normal = glm::normalize(glm::cross(v1, v2));
  intersection.normal = normal;

  return intersection;
}

void acgm::OctreeNode::AddTriangle(const std::vector<glm::vec3> &triangle)
{
  auto half_size = size_ / 2.0f;

  if (!is_limit_ || level_ == level)
  {
    traingles_.reserve(traingles_.size() + 1);
    traingles_.push_back(triangle);

    if (traingles_.size() >= limit && level_ < level)
    {
      is_limit_ = true;
      for (std::vector<glm::vec3> tri : traingles_)
      {
        const auto point0 = tri.at(0);
        const auto point1 = tri.at(1);
        const auto point2 = tri.at(2);

        for (uint8_t i = 0; i < 8; i++)
        {
          if (IsTriangleInAABB(point0, point1, point2, child_center_.at(i), half_size))
          {
            if (child_.at(i) == 0)
            {
              child_.at(i) = std::make_shared<OctreeNode>(child_center_.at(i), half_size, level_ + 1, shared_from_this());
            }
            child_.at(i)->AddTriangle(tri);
          }
        }
      }
      traingles_.clear();
    }
  }
  else
  {
    const auto point0 = triangle.at(0);
    const auto point1 = triangle.at(1);
    const auto point2 = triangle.at(2);

    for (uint8_t i = 0; i < 8; i++)
    {
      if (IsTriangleInAABB(point0, point1, point2, child_center_.at(i), half_size))
      {
        if (child_.at(i) == 0)
        { child_.at(i) = std::make_shared<OctreeNode>(child_center_.at(i), half_size, level_ + 1, shared_from_this()); }

        child_.at(i)->AddTriangle(triangle);
      }
    }
  }
}

bool acgm::OctreeNode::IsPointInAABB(const glm::vec3 &point, const glm::vec3 &center, const glm::vec3 &size) const
{
  const auto min = center - size;
  const auto max = center + size;

  return point.x >= min.x && point.x <= max.x &&
    point.y >= min.y && point.y <= max.y &&
    point.z >= min.z && point.z <= max.z;
}

bool acgm::OctreeNode::AABBIntersection(Ray &ray, const glm::vec3 &center, const glm::vec3 &size) const
{
  const auto start = ray.GetOrigin();
  const auto dir = ray.GetDirection();

  if (IsPointInAABB(start, center, size))
  {
    return true;
  }

  const auto min = center - size;
  const auto max = center + size;

  float t[6];
  t[0] = (min.x - start.x) / dir.x;
  t[1] = (min.y - start.y) / dir.y;
  t[2] = (min.z - start.z) / dir.z;
  t[3] = (max.x - start.x) / dir.x;
  t[4] = (max.y - start.y) / dir.y;
  t[5] = (max.z - start.z) / dir.z;

  glm::vec3 temp;

  for (int i = 0; i < 6; i++)
  {
    if (t[i] > std::numeric_limits<float>::max() || t[i] < 0.0)
    { continue; }

    temp = start + t[i] * dir;

    if ((i % 3) == 0)
    {
      if (glm_ext::IsGequal(temp.y, min.y) && glm_ext::IsLequal(temp.y, max.y) &&
        glm_ext::IsGequal(temp.z, min.z) && glm_ext::IsLequal(temp.z, max.z))
      {
        return true;
      }
    }

    if ((i % 3) == 1)
    {
      if (glm_ext::IsGequal(temp.x, min.x) && glm_ext::IsLequal(temp.x, max.x) &&
        glm_ext::IsGequal(temp.z, min.z) && glm_ext::IsLequal(temp.z, max.z))
      {
        return true;
      }
    }

    if ((i % 3) == 2)
    {
      if (glm_ext::IsGequal(temp.x, min.x) && glm_ext::IsLequal(temp.x, max.x) &&
        glm_ext::IsGequal(temp.y, min.y) && glm_ext::IsLequal(temp.y, max.y))
      {
        return true;
      }
    }
  }

  return false;
}

bool acgm::OctreeNode::IsTriangleInAABB(const glm::vec3 &point0, const glm::vec3 &point1, const glm::vec3 &point2, const glm::vec3 &center, const glm::vec3 &size)
{
  if (IsPointInAABB(point0, center, size))
  {
    return true;
  }

  if (IsPointInAABB(point1, center, size))
  {
    return true;
  }

  if (IsPointInAABB(point2, center, size))
  {
    return true;
  }

  glm::vec3 v[3];
  v[0] = point0 - center;
  v[1] = point1 - center;
  v[2] = point2 - center;

  glm::vec3 e[3];
  e[0] = glm::vec3(1, 0, 0);
  e[1] = glm::vec3(0, 1, 0);
  e[2] = glm::vec3(0, 0, 1);

  glm::vec3 n[3];
  n[0] = v[1] - v[0];
  n[1] = v[2] - v[1];
  n[2] = v[0] - v[2];

  Ray test_ray0(point0);
  test_ray0.SetDirection(n[0]);

  if (AABBIntersection(test_ray0, center, size))
  {
    return true;
  }

  Ray test_ray1(point1);
  test_ray1.SetDirection(n[1]);

  if (AABBIntersection(test_ray1, center, size))
  {
    return true;
  }

  Ray test_ray2(point2);
  test_ray2.SetDirection(n[2]);

  if (AABBIntersection(test_ray2, center, size))
  {
    return true;
  }

  for (uint8_t i = 0; i < 3; i++)
  {
    for (uint8_t j = 0; j < 3; j++)
    {
      auto a = glm::cross(e[i], n[j]);

      const auto point0 = glm::dot(a, v[0]);
      const auto point1 = glm::dot(a, v[1]);
      const auto point2 = glm::dot(a, v[2]);

      const auto min = glm::min(glm::min(point0, point1), point2);
      const auto max = glm::max(glm::max(point0, point1), point2);

      auto x = glm_ext::IsLess(a.x, 0.0) ? -a.x : a.x;
      auto y = glm_ext::IsLess(a.y, 0.0) ? -a.y : a.y;
      auto z = glm_ext::IsLess(a.z, 0.0) ? -a.z : a.z;

      a = glm::vec3(x, y, z);

      const auto rad = glm::dot(size, a);

      if (glm_ext::IsGreater(min, rad) || glm_ext::IsLess(max, -rad))
      { return false; }
    }
  }

  return true;
}



void acgm::Octree::SetupOctree(const cogs::Mesh &mesh)
{
  mesh_ = mesh;

  SetCenterAndSize();

  const auto positions = mesh_.points->GetPositions();

  for (uint32_t i = 0; i < mesh_.faces->GetFaceCount(); i++)
  {
    const auto face = mesh_.faces->GetFaces().at(i);

    const auto point0 = positions[face.x];
    const auto point1 = positions[face.y];
    const auto point2 = positions[face.z];

    std::vector<glm::vec3> triangle;

    triangle.reserve(3);
    triangle.push_back(point0);
    triangle.push_back(point1);
    triangle.push_back(point2);

    AddTriangle(triangle);
  }
}

const std::optional<acgm::HitResult> acgm::Octree::Intersection(Ray &ray)
{
  return root_->Intersection(ray);
}

void acgm::Octree::AddTriangle(std::vector<glm::vec3> &triangle)
{
  root_->AddTriangle(triangle);
}

void acgm::Octree::SetCenterAndSize()
{
  constexpr auto inf = std::numeric_limits<float>::max();
  auto min = glm::vec3(inf, inf, inf);
  auto max = -glm::vec3(inf, inf, inf);

  auto positions = mesh_.points->GetPositions();

  for (uint32_t i = 0; i < mesh_.points->GetSize(); i++)
  {
    if (glm_ext::IsLess(positions[i].x, min.x))
    {
      min.x = positions[i].x;
    }
    if (glm_ext::IsLess(positions[i].y, min.y))
    {
      min.y = positions[i].y;
    }
    if (glm_ext::IsLess(positions[i].z, min.z))
    {
      min.z = positions[i].z;
    }

    if (glm_ext::IsGreater(positions[i].x, max.x))
    {
      max.x = positions[i].x;
    }
    if (glm_ext::IsGreater(positions[i].y, max.y))
    {
      max.y = positions[i].y;
    }
    if (glm_ext::IsGreater(positions[i].z, max.z))
    {
      max.z = positions[i].z;
    }
  }

  auto center = glm::vec3((max.x + min.x) / 2, (max.y + min.y) / 2, (max.z + min.z) / 2);
  auto size = glm::vec3((max.x - min.x) / 2, (max.y - min.y) / 2, (max.z - min.z) / 2);

  root_ = std::make_shared<OctreeNode>(center, size, 0);
}
