#include <glm/glm.hpp>
#include <Utils/ExtGLM.h>
#include <ACGM_RayTracer_lib/PointLight.h>



acgm::PointLight::PointLight(const float &intensity, const glm::vec3 &position, const uint16_t &range, const float &linear_att, const float &quadratic)
  : Light(intensity),
    position_(position),
    range_(range),
    linear_att_(linear_att),
    quadratic_att_(quadratic)
{

}

glm::vec3 acgm::PointLight::GetDirectionToLight(const glm::vec3 &point) const
{
  return glm::normalize(position_ - point);
}

float acgm::PointLight::GetLightIntensity(const glm::vec3 &point) const
{
  const float distance = this->CalculateDistance(position_, point);
  const float l = (range_) / (range_ + (distance * linear_att_));
  const float q = pow(range_, 2) / (pow(range_, 2) + distance * distance * quadratic_att_);

  const float intensity = l * q * this->GetIntensity();

  return intensity;
}

float acgm::PointLight::GetDistanceToLight(const glm::vec3 &hit_point) const
{
  return this->CalculateDistance(position_, hit_point);
}
