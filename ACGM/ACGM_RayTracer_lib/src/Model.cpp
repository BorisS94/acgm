#include <ACGM_RayTracer_lib/Model.h>
#include <glm/glm.hpp>



acgm::Model::Model()
{
}

void acgm::Model::SetShader(const std::shared_ptr<Shader> &shader)
{
  shader_ = shader;
}

const std::shared_ptr<acgm::Shader> acgm::Model::GetShader() const
{
  return shader_;
}
