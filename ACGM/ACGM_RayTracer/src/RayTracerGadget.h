#pragma once

#include <HIRO/Gadget.h>
#include <HIRO_DRAW/renderers/RasterRenderer.h>

#include <ACGM_RayTracer_lib/Scene.h>



//! Visual part of the RayTracer HIRO module.
class RayTracerGadget
  : public hiro::Gadget
{
public:
  //! Construct with a HIRO resource.
  explicit RayTracerGadget(const hiro::Resource *res);

  void Initialize() override;
  void GenerateGui(hiro::GuiGenerator &gui) override;

private:
  //! Structure specifying how the raster should be rendered.
  hiro::draw::PRasterStyle style_;

  std::shared_ptr<acgm::Scene> scene_;

  bool use_octree_ = true;

  uint8_t max_reflections_ = 10;
  uint8_t max_refractions_ = 10;

  hiro::gui::Droplist *scene_selector_;

  void CreateScene();
};
