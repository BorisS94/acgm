#include "RayTracerResource.h"
#include "RayTracerGadget.h"

#include <ACGM_RayTracer_lib/SceneImporter.h>



RayTracerGadget::RayTracerGadget(const hiro::Resource *res)
  : hiro::Gadget(res)
{
}

void RayTracerGadget::Initialize()
{
  hiro::Gadget::Initialize();
  style_ = std::make_shared<hiro::draw::RasterStyle>();
  style_->use_nearest_filtering = true;
  AddRenderer(GetResource<RayTracerResource>()->GetRenderer(), style_);
}

void RayTracerGadget::GenerateGui(hiro::GuiGenerator &gui)
{
  gui.AddCheckbox("Use Octree")
  ->Set(use_octree_)
  ->Subscribe([this](const hiro::gui::Checkbox * checkbox)
  {
    use_octree_ = checkbox->Get() ? true : false;
  });

  gui.AddNumericInt("Max reflections")
  ->SetMax(10)
  ->SetMin(0)
  ->Set(10)
  ->Subscribe([this](const hiro::gui::NumericInt * numeric_int)
  {
    max_reflections_ = numeric_int->Get();
  });

  gui.AddNumericInt("Max refractions")
  ->SetMax(10)
  ->SetMin(0)
  ->Set(10)
  ->Subscribe([this](const hiro::gui::NumericInt * numeric_int)
  {
    max_refractions_ = numeric_int->Get();
  });

  scene_selector_ = gui.AddDroplist("Scene")
    ->AddItemsIndexed({ "scene0.txt", "scene1.txt", "scene2.txt", "scene3.txt", "scene4.txt", "scene5.txt", "scene6.txt", "scene7.txt", "scene8.txt" })
    ->Set(0);

  gui.AddButton("Raytrace scene")
  ->Subscribe([this](const hiro::gui::Button *)
  {
    CreateScene();
  });
}

void RayTracerGadget::CreateScene()
{
  acgm::SceneImporter importer;
  importer.SetOctree(use_octree_);
  importer.Import(scene_selector_->GetText());
  scene_ = importer.GetScene();
  scene_->SetMaxReflections(max_reflections_);
  scene_->SetMaxRefractions(max_refractions_);
  const auto options = importer.GetRenderOptions();
  GetResource<RayTracerResource>()->GetRenderer()->SetResolution({ options.resolution });
  // Raytrace the scene
  scene_->Raytrace(*GetResource<RayTracerResource>()->GetRenderer());
}
