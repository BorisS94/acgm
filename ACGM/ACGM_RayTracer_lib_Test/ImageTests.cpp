#include "pch.h"
#include <glm/gtc/epsilon.hpp>
#include <ACGM_RayTracer_lib/Image.h>



const auto image = std::make_shared<acgm::Image>("test.jpg", glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(-0.707107f, 0.707107f, 0.0f));



TEST(ImageTests, GetColorAtZero)
{
  const auto color = image->GetColorAt(glm::vec2(0.0f, 0.0f));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.71372550f,
      color.r,
      glm::epsilon<float>()
    ));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.65098041f,
      color.g,
      glm::epsilon<float>()
    ));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.52156865f,
      color.b,
      glm::epsilon<float>()
    ));
}

TEST(ImageTests, GetColorAtMiddle)
{
  const auto color = image->GetColorAt(glm::vec2(0.5f, 0.5f));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      1.0f,
      color.r,
      glm::epsilon<float>()
    ));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      1.0f,
      color.g,
      glm::epsilon<float>()
    ));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      1.0f,
      color.b,
      glm::epsilon<float>()
    ));
}

TEST(ImageTests, GetColorAtRandom)
{
  const auto color = image->GetColorAt(glm::vec2(0.15f, 0.25f));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.57647061f,
      color.r,
      glm::epsilon<float>()
    ));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.49411764f,
      color.g,
      glm::epsilon<float>()
    ));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.37254902f,
      color.b,
      glm::epsilon<float>()
    ));
}

TEST(ImageTests, GetColorAtOne)
{
  const auto color = image->GetColorAt(glm::vec2(0.99f, 0.99f));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.2862745225f,
      color.r,
      glm::epsilon<float>()
    ));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.2509804069f,
      color.g,
      glm::epsilon<float>()
    ));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.1843137294f,
      color.b,
      glm::epsilon<float>()
    ));
}
