#include "pch.h"
#include <glm/gtc/epsilon.hpp>

#include <ACGM_RayTracer_lib/Model.h>
#include <ACGM_RayTracer_lib/Plane.h>
#include <ACGM_RayTracer_lib/BlinnPhongShader.h>
#include <ACGM_RayTracer_lib/CheckerboardShader.h>



TEST(ShaderTests, BlinnPhongTest)
{
  acgm::ShaderParams params;
  params.ambient = 0.5f;
  params.diffuse = 0.75f;
  params.specular = 0.25f;
  params.shininess = 50;
  acgm::MaterialParams mat_params;
  const auto phong_yellow = std::make_shared<acgm::BlinnPhongShader>(color::YELLOW, params, mat_params);

  auto plane = std::make_shared<acgm::Plane>(glm::vec3(0.0f, 0.0f, -0.3f), glm::vec3(0.0f, 0.0f, 1.0f));
  plane->SetShader(phong_yellow);

  acgm::ShaderInput shader_input;
  shader_input.point = glm::vec3(0.3f, 0.0f, 0.8f);
  shader_input.normal = glm::vec3(0.0f, 0.0f, 1.0f);
  shader_input.dir_to_camera = -glm::vec3(0.54f, 0.23f, -0.48f);
  shader_input.dir_to_light = glm::vec3(-0.84f, 0.11f, 0.02f);
  shader_input.intensity = 1.0f;

  const auto shader = plane->GetShader();
  const auto color = shader->CalculateColor(shader_input).first;

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.515f,
      color.r,
      glm::epsilon<float>()
    ));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.515f,
      color.g,
      glm::epsilon<float>()
    ));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.0f,
      color.b,
      glm::epsilon<float>()
    ));
}

TEST(ShaderTests, CheckerBoardTest)
{
  acgm::ShaderParams params;
  params.ambient = 0.5f;
  params.diffuse = 0.75f;
  params.specular = 0.25f;
  params.shininess = 50;
  acgm::MaterialParams mat_params;
  const auto phong_cyan = std::make_shared<acgm::BlinnPhongShader>(color::GREY, params, mat_params);
  const auto phong_yellow = std::make_shared<acgm::BlinnPhongShader>(color::YELLOW, params, mat_params);
  const auto checkerboard = std::make_shared<acgm::CheckerBoardShader>(phong_cyan, phong_yellow, 1.0f);

  auto plane = std::make_shared<acgm::Plane>(glm::vec3(0.0f, 0.0f, -0.3f), glm::vec3(0.0f, 0.0f, 1.0f));
  plane->SetShader(checkerboard);

  acgm::ShaderInput shader_input;
  shader_input.point = glm::vec3(0.3f, 0.0f, 0.8f);
  shader_input.normal = glm::vec3(0.0f, 0.0f, 1.0f);
  shader_input.dir_to_camera = -glm::vec3(0.54f, 0.23f, -0.48f);
  shader_input.dir_to_light = glm::vec3(-0.84f, 0.11f, 0.02f);
  shader_input.intensity = 1.0f;

  const auto shader = plane->GetShader();
  const auto color = shader->CalculateColor(shader_input).first;

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.2575f,
      color.r,
      glm::epsilon<float>()
    ));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.2575f,
      color.g,
      glm::epsilon<float>()
    ));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.2575f,
      color.b,
      glm::epsilon<float>()
    ));
}
