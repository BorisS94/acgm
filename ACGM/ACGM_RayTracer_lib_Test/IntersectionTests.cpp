#include "pch.h"

#include <ACGM_RayTracer_lib/Ray.h>
#include <ACGM_RayTracer_lib/Model.h>
#include <ACGM_RayTracer_lib/Plane.h>
#include <ACGM_RayTracer_lib/Mesh.h>
#include <ACGM_RayTracer_lib/Sphere.h>
#include <ACGM_RayTracer_lib/BlinnPhongShader.h>
#include <ACGM_RayTracer_lib/CheckerboardShader.h>



TEST(IntersectionTests, RayPlaneIntersectionTestValue)
{
  acgm::ShaderParams params;
  acgm::MaterialParams mat_params;

  auto plane = std::make_shared<acgm::Plane>(glm::vec3(0.0f, 0.0f, -0.3f), glm::vec3(0.0f, 0.0f, 1.0f));
  plane->SetShader(std::make_shared<acgm::BlinnPhongShader>(color::WHITE, params, mat_params));
  acgm::Ray ray(glm::vec3(0.0f, 1.2f, 0.0f));
  ray.SetDirection(glm::vec3(-0.471046, -0.816692, -0.333363));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.89992f,
      plane->Intersection(ray).value().t,
      glm::epsilon<float>()
    ));
}

TEST(IntersectionTests, RayPlaneIntersectionTestNullopt)
{
  acgm::ShaderParams params;
  acgm::MaterialParams mat_params;
  auto plane = std::make_shared<acgm::Plane>(glm::vec3(0.0f, 0.0f, -0.3f), glm::vec3(0.0f, 0.0f, 1.0f));
  plane->SetShader(std::make_shared<acgm::BlinnPhongShader>(color::WHITE, params, mat_params));
  acgm::Ray ray(glm::vec3(0.0f, 1.2f, 0.0f));
  ray.SetDirection(glm::vec3(-0.492026, -0.858222, 0.146171));

  EXPECT_EQ(std::nullopt, plane->Intersection(ray));
}

cogs::Mesh CreateCube()
{
  auto pc = std::make_shared<cogs::PointCloud>();
  pc->Resize(24);

  auto positions = pc->GetPositions();

  positions[0] = { 1.0f, 1.0f, -1.0f };
  positions[1] = { -1.0f, 1.0f, -1.0f };
  positions[2] = { -1.0f, 1.0f, 1.0f };
  positions[3] = { 1.0f, 1.0f, 1.0f };
  positions[4] = { 1.0f, -1.0f, 1.0f };
  positions[5] = { 1.0f, 1.0f, 1.0f };
  positions[6] = { -1.0f, 1.0f, 1.0f };
  positions[7] = { -1.0f, -1.0f, 1.0f };
  positions[8] = { -1.0f, -1.0f, 1.0f };
  positions[9] = { -1.0f, 1.0f, 1.0f };
  positions[10] = { -1.0f, 1.0f, -1.0f };
  positions[11] = { -1.0f, -1.0f, -1.0f };
  positions[12] = { -1.0f, -1.0f, -1.0f };
  positions[13] = { 1.0f, -1.0f, -1.0f };
  positions[14] = { 1.0f, -1.0f, 1.0f };
  positions[15] = { -1.0f, -1.0f, 1.0f };
  positions[16] = { 1.0f, -1.0f, -1.0f };
  positions[17] = { 1.0f, 1.0f, -1.0f };
  positions[18] = { 1.0f, 1.0f, 1.0f };
  positions[19] = { 1.0f, -1.0f, 1.0f };
  positions[20] = { -1.0f, -1.0f, -1.0f };
  positions[21] = { -1.0f, 1.0f, -1.0f };
  positions[22] = { 1.0f, 1.0f, -1.0f };
  positions[23] = { 1.0f, -1.0f, -1.0f };

  cogs::Mesh mesh;
  mesh.points = pc;

  const std::vector<glm::uvec3> triangles
  {
    { 0, 1, 2 },
    { 0, 2, 3 },
    { 4, 5, 6 },
    { 4, 6, 7 },
    { 8, 9, 10 },
    { 8, 10, 11 },
    { 12, 13, 14 },
    { 12, 14, 15 },
    { 16, 17, 18 },
    { 16, 18, 19 },
    { 20, 21, 22 },
    { 20, 22, 23 }
  };

  mesh.faces = std::make_shared<cogs::Triangulation>();
  mesh.faces->SetFaces(triangles);

  return mesh;
}

TEST(IntersectionTests, RayMeshIntersectionTestValue)
{
  cogs::Transform transform;
  transform.SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
  transform.SetScaling(glm::vec3(0.2f, 0.2f, 0.2f));
  transform.SetRotation(glm::quat(glm::vec3((glm::radians(0.0f), glm::radians(0.0f), glm::radians(0.0f)))));

  acgm::ShaderParams params;
  acgm::MaterialParams mat_params;
  auto mesh = std::make_shared<acgm::Mesh>(CreateCube(), transform, std::make_shared<acgm::Octree>());
  mesh->SetShader(std::make_shared<acgm::BlinnPhongShader>(color::YELLOW, params, mat_params));

  acgm::Ray ray(glm::vec3(0.0f, 1.2f, 0.0f));
  ray.SetDirection(glm::vec3(-0.168232, -0.971286, 0.168232));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      1.029563f,
      mesh->Intersection(ray).value().t,
      glm::epsilon<float>()
    ));
}

TEST(IntersectionTests, RayMeshIntersectionTestNullopt)
{
  cogs::Transform transform;
  transform.SetPosition(glm::vec3(0.0f, 0.0f, -0.3f));
  transform.SetScaling(glm::vec3(0.2f, 0.2f, 0.2f));
  transform.SetRotation(glm::quat(glm::vec3((glm::radians(0.0f), glm::radians(0.0f), glm::radians(0.0f)))));

  acgm::ShaderParams params;
  acgm::MaterialParams mat_params;
  auto mesh = std::make_shared<acgm::Mesh>(CreateCube(), transform, std::make_shared<acgm::Octree>());
  mesh->SetShader(std::make_shared<acgm::BlinnPhongShader>(color::YELLOW, params, mat_params));

  acgm::Ray ray(glm::vec3(0.0f, 0.5f, 2.0f));
  ray.SetDirection(glm::vec3(0.471046, 0.816692, 0.333363));

  EXPECT_EQ(std::nullopt, mesh->Intersection(ray));
}

TEST(IntersectionTests, RaySphereIntersectionTestValue)
{
  acgm::ShaderParams params;
  acgm::MaterialParams mat_params;
  auto sphere = std::make_shared<acgm::Sphere>(glm::vec3(0.0f, 0.5f, 0.0f), 0.5f);
  sphere->SetShader(std::make_shared<acgm::BlinnPhongShader>(color::WHITE, params, mat_params));
  acgm::Ray ray(glm::vec3(0.0f, 1.2f, 0.0f));
  ray.SetDirection(glm::vec3(-0.471046, -0.816692, -0.333363));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.277027f,
      sphere->Intersection(ray).value().t,
      glm::epsilon<float>()
    ));
}

TEST(IntersectionTests, RaySphereIntersectionTestNullopt)
{
  acgm::ShaderParams params;
  auto sphere = std::make_shared<acgm::Sphere>(glm::vec3(0.0f, 0.0f, -0.3f), 2.0f);
  acgm::MaterialParams mat_params;
  sphere->SetShader(std::make_shared<acgm::BlinnPhongShader>(color::WHITE, params, mat_params));
  acgm::Ray ray(glm::vec3(0.0f, 0.5f, 2.0f));
  ray.SetDirection(glm::vec3(-0.471046, -0.816692, -0.333363));

  EXPECT_EQ(std::nullopt, sphere->Intersection(ray));
}

