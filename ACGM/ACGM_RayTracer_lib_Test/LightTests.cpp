#include "pch.h"
#include <glm/gtc/epsilon.hpp>

#include <ACGM_RayTracer_lib/DirectionalLight.h>
#include <ACGM_RayTracer_lib/PointLight.h>
#include <ACGM_RayTracer_lib/SpotLight.h>



TEST(LightTests, DirectionalLightTest)
{
  auto dir_light = std::make_shared<acgm::DirectionalLight>(0.8f, glm::vec3(0.0f, 0.0f, -1.0f));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.8f,
      dir_light->GetLightIntensity(glm::vec3(1.0f, 1.0f, 1.0f)),
      glm::epsilon<float>()
    ));

  dir_light->SetIntensity(0.55f);

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.55f,
      dir_light->GetLightIntensity(glm::vec3(1.0f, 1.0f, 1.0f)),
      glm::epsilon<float>()
    ));
}

TEST(LightTests, PointLightTest)
{
  auto point_light = std::make_shared<acgm::PointLight>(1.0f, glm::vec3(0.0f, 0.0f, 2.5f), 5, 0.2f, 0.5f);

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.8514468f,
      point_light->GetLightIntensity(glm::vec3(1.0f, 1.0f, 1.0f)),
      glm::epsilon<float>()
    ));
}

TEST(LightTests, SpotLightTest)
{
  auto spot_light = std::make_shared<acgm::SpotLight>(1.0f, glm::vec3(0.0f, 0.0f, 1.0f), 5, 0.2f, 0.5f, glm::vec3(0.0f, 0.0f, -1.0f), 25, 5);

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.9426847f,
      spot_light->GetLightIntensity(glm::vec3(0.0f, 0.0f, 0.0f)),
      glm::epsilon<float>()
    ));
}
