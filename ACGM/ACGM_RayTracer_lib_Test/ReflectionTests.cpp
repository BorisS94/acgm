#include "pch.h"
#include <glm/gtc/epsilon.hpp>
#include <COGS/Color.h>

#include <ACGM_RayTracer_lib/Ray.h>
#include <ACGM_RayTracer_lib/BlinnPhongShader.h>
#include <ACGM_RayTracer_lib/CheckerboardShader.h>



TEST(ReflectionTests, ReflectedDirectionZero)
{
  acgm::ShaderParams params;
  acgm::MaterialParams mat_params;
  const auto shader = std::make_shared<acgm::BlinnPhongShader>(cogs::Color3f(0.5f, 0.5f, 0.5f), params, mat_params);

  const auto reflected_ray = shader->Reflection(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), 0.0001f);

  const auto dir = reflected_ray.GetDirection();

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.0f,
      dir.x,
      glm::epsilon<float>()
    ));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.0f,
      dir.y,
      glm::epsilon<float>()
    ));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.0f,
      dir.z,
      glm::epsilon<float>()
    ));
}

TEST(ReflectionTests, ReflectedDirectionRandom)
{
  acgm::ShaderParams params;
  acgm::MaterialParams mat_params;
  const auto b_shader = std::make_shared<acgm::BlinnPhongShader>(cogs::Color3f(0.5f, 0.5f, 0.5f), params, mat_params);
  const auto shader = std::make_shared<acgm::CheckerBoardShader>(b_shader, b_shader, 1.0f);

  const auto reflected_ray = shader->Reflection(-glm::vec3(0.2f, 0.4f, 0.1f), glm::vec3(0.01f, 0.2f, 0.8f), glm::vec3(0.4f, 0.2f, 0.9f), 0.0001f);

  const auto dir = reflected_ray.GetDirection();

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.0f,
      dir.x,
      glm::epsilon<float>()
    ));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      -0.3f,
      dir.y,
      glm::epsilon<float>()
    ));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.35f,
      dir.z,
      glm::epsilon<float>()
    ));
}

TEST(ReflectionTests, ReflectedDirectionRandomAllPos)
{
  acgm::ShaderParams params;
  acgm::MaterialParams mat_params;
  const auto shader = std::make_shared<acgm::BlinnPhongShader>(cogs::Color3f(0.5f, 0.5f, 0.5f), params, mat_params);

  const auto reflected_ray = shader->Reflection(glm::vec3(0.43f, 0.27f, 0.36f), glm::vec3(0.89f, 0.37f, 0.27f), glm::vec3(0.56f, 0.19f, 0.27f), 0.0001f);

  const auto dir = reflected_ray.GetDirection();

  EXPECT_TRUE(glm::epsilonEqual<float>(
      -0.006016f,
      dir.x,
      glm::epsilon<float>()
    ));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.122066f,
      dir.y,
      glm::epsilon<float>()
    ));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.149778f,
      dir.z,
      glm::epsilon<float>()
    ));
}

TEST(ReflectionTests, ReflectedDirectionRandomAllNeg)
{
  acgm::ShaderParams params;
  acgm::MaterialParams mat_params;
  const auto shader = std::make_shared<acgm::BlinnPhongShader>(cogs::Color3f(0.5f, 0.5f, 0.5f), params, mat_params);

  const auto reflected_ray = shader->Reflection(-glm::vec3(0.25f, 0.48f, 0.11f), -glm::vec3(0.61f, 0.42f, 0.54f), -glm::vec3(0.34f, 0.23f, 0.25f), 0.0001f);

  const auto dir = reflected_ray.GetDirection();

  EXPECT_TRUE(glm::epsilonEqual<float>(
      -0.098428f,
      dir.x,
      glm::epsilon<float>()
    ));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      -0.377466f,
      dir.y,
      glm::epsilon<float>()
    ));

  EXPECT_TRUE(glm::epsilonEqual<float>(
      0.001450f,
      dir.z,
      glm::epsilon<float>()
    ));
}
